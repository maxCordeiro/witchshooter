﻿using UnityEngine;
using System.Collections;

public static class GameUtility
{
    public static float Approach(float arg0, float arg1, float arg2)
    {
        if (arg0 < arg1)
        {
            return Mathf.Min(arg0 + arg2, arg1);
        }
        else
        {
            return Mathf.Max(arg0 - arg2, arg1);
        }
    }
    public static bool InView(Vector3 pos)
    {
        Camera cam = Camera.main;

        float camVert = cam.orthographicSize;
        float camHorz = camVert * Screen.width / Screen.height;

        return pos.x < cam.transform.position.x + camHorz && pos.x > cam.transform.position.x + -camHorz
            && pos.y < cam.transform.position.y + camVert && pos.y > cam.transform.position.y + -camVert;
    }

    public static GameObject SpawnEnemy(EnemyType enemy)
    {
        float spawnAngle = Random.Range(0f, Mathf.PI);

        Vector3 spawnPos = new Vector3(Mathf.Sin(spawnAngle), Mathf.Cos(spawnAngle), 0);

        spawnPos *= 3;

        EnemyController e = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Enemies/" + enemy.ToString()), spawnPos, Quaternion.identity).GetComponent<EnemyController>();

        EmissionsManager.instance.spawnedEnemies++;
        GameManager.instance.gameCurrentSpawns++;

        return e.gameObject;
    }

    public static int GetSpellCost(WandTypes spell)
    {
        int c = 0;

        switch (spell)
        {
            case WandTypes.BURST:
                c = 200;
                break;
            case WandTypes.HOMING:
                c = 400;
                break;
            case WandTypes.LARGE:
                c = 300;
                break;
            case WandTypes.PINGPONG:
                c = 350;
                break;
            case WandTypes.THROUGH:
                c = 250;
                break;
            case WandTypes.TRIPLE:
                c = 350;
                break;
            default:
                break;
        }

        c = Mathf.RoundToInt((c * GameManager.instance.gameCurrentFloor * 0.66f) / 5) * 5;

        return c;
    }

    public static WandData LoadWandData(WandTypes wand)
    {
        string path = "ScriptableObjects/Wands/" + wand.ToString();

        Debug.Log(path);
        return Resources.Load<WandData>(path);
    }
}
