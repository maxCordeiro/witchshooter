﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "New EnemyMovement_IDLE", menuName = "WitchGame/EnemyMovement/IDLE")]
public class EnemyMovement_IDLE : EnemyMovement
{
}