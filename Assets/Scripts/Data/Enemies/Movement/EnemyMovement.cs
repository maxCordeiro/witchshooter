﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : ScriptableObject
{
    public Enemy_Movement moveType;

    public float speed;
}