﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyData : ScriptableObject
{
    public EnemyType id;

    public float baseSpeed;
    public float baseHP;
    public float baseDMG;

    public EnemyController controller;
}

public enum Enemy_Movement
{
    NONE,
    ROAM,
    FOLLOWPLAYER
}

public enum Enemy_Hit
{
    NONE,
    CHANGEMOVEMENT
}