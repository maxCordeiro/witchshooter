﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Wand", menuName = "WitchGame/Create Wand")]
public class WandData : ScriptableObject
{
    public WandTypes id;

    public float baseCooldown = 0.4f;
    public float baseSpeed = 5;

    public int overrideBaseDamage = -1;

    public Sprite GetIcon()
    {
        Sprite s = Resources.Load<Sprite>("Wands/wand_normal");

        return s;
    }

    public Sprite GetSprite()
    {
        Sprite s = Resources.Load<Sprite>("Wands/wand_normal");

        return s;
    }
}
