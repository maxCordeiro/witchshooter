﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharger : EnemyController
{
    public bool canRoam;
    public float roamingTimer, pauseTimer;

    public float roamLength, pauseLength;
    public float startTimer, startPauseLength;

    public bool isCharging;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        SetHP(20);
        charContr.onTriggerEnterEvent += HurtPlayer;
        charContr.onTriggerEnterEvent += (Collider2D c) => {
            if (c.gameObject != player.gameObject) return;
            isCharging = false;
            moveSpeed = 0.75f;
        };
        onTakeDamageEvent += Charge;
    }

    public void Update()
    {
        if (!isCharging)
        {
            if (canRoam)
            {
                roamingTimer += Time.deltaTime;
                if (roamingTimer >= roamLength)
                {
                    roamingTimer = 0;
                    canRoam = false;
                }
            }
            else
            {
                pauseTimer += Time.deltaTime;
                if (pauseTimer >= pauseLength)
                {
                    pauseTimer = 0;
                    canRoam = true;
                    velocity = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
                }
            }
        } else
        {
            GetPlayerTargetVelocity();
        }
    }

    public void Charge()
    {
        moveSpeed = PlayerManager.instance.playerMaxSpeed - 0.25f;
        isCharging = true;
    }

    private void FixedUpdate()
    {
        if (canRoam || isCharging) charContr.move(velocity * moveSpeed * Time.deltaTime);
    }
}
