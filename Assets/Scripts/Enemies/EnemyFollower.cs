﻿using UnityEngine;
using System.Collections;

public class EnemyFollower : EnemyController
{
    public bool canRoam;
    public float startTimer, startPauseLength;

    public override void Start()
    {
        base.Start();

        SetHP(25);

        charContr.onTriggerEnterEvent += HurtPlayer;
        charContr.onTriggerEnterEvent += (Collider2D c) => { canRoam = false; };
    }

    public void Update()
    {
        GetPlayerTargetVelocity();

        if (!canRoam)
        {
            startTimer += Time.deltaTime;
            if (startTimer >= startPauseLength)
            {
                startTimer = 0;
                canRoam = true;
            }
        }
    }

    private void FixedUpdate()
    {
        if (canRoam) charContr.move(velocity * moveSpeed * Time.deltaTime);
    }
}
