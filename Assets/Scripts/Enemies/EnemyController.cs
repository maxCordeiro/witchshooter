﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31;

public class EnemyController : MonoBehaviour
{
    [Header("Internal Variables")]
    public int HP;
    public int TotalHP;

    public int scoreToGive;

    public PlayerController player;
    public CharacterController2D charContr;

    public Vector2 velocity;

    [Header("Enemy Variables")]
    public float moveSpeed;
    public EnemyType enemyType;

    public virtual void Start()
    {
        player = FindObjectOfType<PlayerController>();
        charContr = GetComponent<CharacterController2D>();

        SetScore(50);

        onDieEvent += GiveScore;
    }

    public void Hurt(int dmg)
    {
        HP -= dmg;
        
        if (HP <= 0)
        {
            Die();
        } else
        {
            OnTakeDamage();
        }
    }

    public void SetHP(int amt)
    {
        TotalHP = amt;
        HP = TotalHP;
    }

    public void SetScore(int scoreAmt)
    {
        scoreToGive = scoreAmt;
    }

    public void Die()
    {
        OnDie();

        EmissionsManager.instance.spawnedEnemies--;

        Destroy(gameObject);
    }

    public void GiveScore()
    {
        GameManager.instance.gameScore += Mathf.RoundToInt(scoreToGive * GameManager.instance.gameScoreMultiplier);
        PlayerManager.instance.UpdateHUD_Score();
    }

    public event Action onTakeDamageEvent;
    public event Action onDieEvent;

    public void OnTakeDamage() => onTakeDamageEvent?.Invoke();
    public void OnDie() => onDieEvent?.Invoke();

    public void HurtPlayer(Collider2D col)
    {
        if (col.gameObject != player.gameObject) return;

        PlayerController p = col.GetComponent<PlayerController>();

        p.Hurt();
    }

    public void GetPlayerTargetVelocity() => velocity = (player.transform.position - transform.position).normalized;

}

public enum EnemyType
{
    NONE = -1,
    CHARGER,
    BURST,
    FOLLOWER
}