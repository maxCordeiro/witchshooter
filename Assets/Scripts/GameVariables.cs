﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class GameVariables
{
    public static List<FloorSpawns> FLOORSPAWNS = new List<FloorSpawns>()
    {
        new FloorSpawns(EnemyType.FOLLOWER, EnemyType.CHARGER),
    };

    public static Dictionary<WandTypes, int> SHOTDMG = new Dictionary<WandTypes, int>()
    {
       { WandTypes.NORMAL, 5 },
    };
}

public class FloorSpawns
{
    public List<EnemyType> spawns;

    public FloorSpawns(params EnemyType[] enemies)
    {
        spawns = enemies.ToList();
    }
}