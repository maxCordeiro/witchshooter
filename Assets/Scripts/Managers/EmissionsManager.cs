﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmissionsManager : MonoBehaviour
{
    private static EmissionsManager _instance;
    public static EmissionsManager instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(EmissionsManager)) as EmissionsManager;
                if (!_instance)
                    Debug.Log("There need to be at least one active EmissionsManager on the scene");
            }

            return _instance;
        }
    }

    public int spawnedEnemies;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.gameInProgress || FloorManager.instance.isSwitchingFloors) return;

        if (spawnedEnemies < GameManager.instance.gameMaxActiveEnemies && GameManager.instance.gameCurrentSpawns < GameManager.instance.gameMaxSpawns)
        {
            int listIndex = Mathf.FloorToInt(GameManager.instance.gameCurrentFloor / 10);
            int spawnIndex = Random.Range(0, GameVariables.FLOORSPAWNS[listIndex].spawns.Count);

            EnemyType toSpawn = GameVariables.FLOORSPAWNS[listIndex].spawns[spawnIndex];

            GameUtility.SpawnEnemy(toSpawn);
        }
    }
}
