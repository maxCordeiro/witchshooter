﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(GameManager)) as GameManager;
                if (!_instance)
                    Debug.Log("There need to be at least one active GameManager on the scene");
            }

            return _instance;
        }
    }

    [Header("Game Variables")]
    public bool gameInProgress;
    public bool gameInStore;

    public int gameScore;
    public float gameScoreMultiplier;

    public int gameCurrentFloor;

    //public int gameCurrentWave;
    public int gameMaxActiveEnemies;
    public int gameMaxSpawns;
    public int gameCurrentSpawns;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GameStart()
    {
        Debug.Log("Game starting!");

        PlayerManager.instance.playerTotalHearts = 6;
        PlayerManager.instance.playerCurrHearts = PlayerManager.instance.playerTotalHearts;

        PlayerManager.instance.playerWand = GameUtility.LoadWandData(WandTypes.NORMAL);
        PlayerManager.instance.playerTome = TomeTypes.NONE;

        PlayerManager.instance.playerMaxSpeed = 5;

        PlayerManager.instance.playerWandTimer = PlayerManager.instance.playerWand.baseCooldown;

        PlayerManager.instance.baseDMG = 5;

        gameCurrentFloor = 0;
        //GameManager.instance.gameCurrentWave = 0;
        gameMaxActiveEnemies = 2;
        gameMaxSpawns = 4;
        gameScoreMultiplier = 1;

        PlayerManager.instance.UpdateHUD();

        gameInProgress = true;
    }
}
