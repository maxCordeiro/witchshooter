﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour
{
    private static FloorManager _instance;
    public static FloorManager instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(FloorManager)) as FloorManager;
                if (!_instance)
                    Debug.Log("There need to be at least one active FloorManager on the scene");
            }

            return _instance;
        }
    }

    public bool isSwitchingFloors;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.gameInProgress) return;

        if (GameManager.instance.gameCurrentSpawns >= GameManager.instance.gameMaxSpawns && EmissionsManager.instance.spawnedEnemies == 0)
        {
            NextFloor();
        }
    }

    public void NextFloor()
    {
        isSwitchingFloors = true;

        GameManager.instance.gameCurrentFloor++;
        //GameManager.instance.gameCurrentWave = 0;
        GameManager.instance.gameCurrentSpawns = 0;
        GameManager.instance.gameMaxActiveEnemies = 2 + Mathf.RoundToInt(GameManager.instance.gameCurrentFloor * 0.66f);
        GameManager.instance.gameMaxSpawns = 5 + Mathf.FloorToInt(GameManager.instance.gameCurrentFloor * 1.45f);

        EmissionsManager.instance.spawnedEnemies = 0;

        ShotController[] shots = FindObjectsOfType<ShotController>();

        foreach (ShotController s in shots)
        {
            Destroy(s.gameObject);
        }

        PlayerManager.instance.UpdateHUD_Floor();

        Debug.Log("Next floor! Floor: " + GameManager.instance.gameCurrentFloor);

        if (GameManager.instance.gameCurrentFloor % 2 == 0)
        {
            ShopManager.instance.StartShop();
        }
        else
        {
            isSwitchingFloors = false;
        }
    }
}
