﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    private static PlayerManager _instance;
    public static PlayerManager instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(PlayerManager)) as PlayerManager;
                if (!_instance)
                    Debug.Log("There need to be at least one active PlayerManager on the scene");
            }

            return _instance;
        }
    }

    [Header("Player Variables")]
    public int playerMaxSpeed;

    public int playerCurrHearts;
    public int playerTotalHearts; //Max 20

    public float playerWandTimer;

    public float playerTomeTimer; // -1 if reset when new floor
    public float playerTomeCooldown;

    public WandData playerWand;
    public TomeTypes playerTome;

    [Header("Wand Variables")]
    public int baseDMG;
    public float DMGmultiplier;

    public float wandSpeedMultiplier;
    public float wandTimerMultiplier;

    [Header("UI")]
    public Sprite[] tomeSprites;
    
    public Image uiShot;
    public Image uiShotCooldown;
    public Image uiTome;
    public Image uiTomeCooldown;

    public SuperTextMesh uiScore;
    public SuperTextMesh uiFloor;

    private void Awake()
    {
        tomeSprites = Resources.LoadAll<Sprite>("UI/tomeSprites");
    }

    public void UpdateHUD()
    {
        UpdateHUD_Weapons();
        UpdateHUD_Cooldowns();
        UpdateHUD_HP();
        UpdateHUD_Score();
        UpdateHUD_Floor();
    }

    public void UpdateHUD_Weapons()
    {
        uiShot.sprite = playerWand.GetIcon();
        uiShotCooldown.sprite = playerWand.GetIcon();

        uiTome.sprite = playerTome == TomeTypes.NONE ? null : tomeSprites[(int)playerTome];
        uiTome.color = playerTome == TomeTypes.NONE ? Color.clear : Color.white;
    }

    public void UpdateHUD_Cooldowns()
    {
        uiShotCooldown.fillAmount = 1 - (playerWandTimer / playerWand.baseCooldown);
    }

    public void UpdateHUD_HP()
    {

    }

    public void UpdateHUD_Score()
    {
        uiScore.text = GameManager.instance.gameScore.ToString("N0");
    }

    public void UpdateHUD_Floor()
    {
        uiFloor.text = "FLOOR " + (GameManager.instance.gameCurrentFloor + 1).ToString();
    }

    public float GetWandSpeed() => playerWand.baseSpeed * (1 + (0.5f * wandSpeedMultiplier));
    public float DecreaseWandTimer() => playerWandTimer -= Time.deltaTime * (1 + (0.5f * wandTimerMultiplier));
}

public enum WandTypes
{
    NORMAL,
    TRIPLE,
    BURST,
    LARGE,
    PINGPONG,
    THROUGH,
    HOMING
}

public enum RobeTypes
{
    A,
    B,
    C,
    D,
    E
}

public enum TomeTypes
{
    NONE = -1,
    WAVE,
    POISON,
    FREEZE,
    SPIRAL,
}

public enum RuneTypes
{

}

public enum ProjectileTypes
{
    PLAYER,
    ENEMY
}

