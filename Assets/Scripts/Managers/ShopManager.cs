﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using UnityEngine.EventSystems;

public class ShopManager : MonoBehaviour
{
    private static ShopManager _instance;
    public static ShopManager instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(ShopManager)) as ShopManager;
                if (!_instance)
                    Debug.Log("There need to be at least one active ShopManager on the scene");
            }

            return _instance;
        }
    }

    public Canvas shopCanvas;

    public Button[] spellButtons;
    public SuperTextMesh[] spellText;
    public SuperTextMesh scoreText, itemName, itemDesc;

    public WandTypes[] spells = new WandTypes[3];
    public int[] spellCost = new int[3];

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.gameInStore) return;

    }

    public void OnButtonSelect(int index)
    {
        if (GameManager.instance.gameScore >= spellCost[index])
        {
            //PlayerManager.instance.playerWand.id = spells[index];
            spellButtons[index].interactable = false;
        }

        CloseShop();
    }

    public void StartShop()
    {
        UpdateScore();
        SelectItems();

        shopCanvas.gameObject.SetActive(true);
        GameManager.instance.gameInStore = true;
    }

    public void CloseShop()
    {
        shopCanvas.gameObject.SetActive(false);

        GameManager.instance.gameInStore = false;
        FloorManager.instance.isSwitchingFloors = false;
    }

    public void SelectItems()
    {
        List<WandTypes> shotPool = Enum.GetValues(typeof(WandTypes)).Cast<WandTypes>().ToList();

        //shotPool.Remove(PlayerManager.instance.playerWand);
        shotPool.Remove(WandTypes.NORMAL);

        for (int i = 0; i < 3; i++)
        {
            int index = Random.Range(0, shotPool.Count);
            spells[i] = shotPool[index];
            shotPool.RemoveAt(index);
            spellCost[i] = GameUtility.GetSpellCost(spells[i]);

            spellText[i].text = spellCost[i].ToString("N0");
        }

        EventSystem.current.SetSelectedGameObject(spellButtons[0].gameObject);
    }

    public void UpdateScore()
    {
        scoreText.text = "SCORE<br>" + GameManager.instance.gameScore.ToString("N0");
    }
}
