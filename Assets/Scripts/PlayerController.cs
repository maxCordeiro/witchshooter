﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31;
using Rewired;

public class PlayerController : MonoBehaviour
{
    CharacterController2D charContr;

    SpriteRenderer cursor, witchRenderer;
    
    [Header("Movement")]
    public Vector2 moveInput;
    public Vector2 moveSpeed;
    public Vector2 velocity;
    public float maxSpeed;
    public float acceleration;

    public Vector3 aimInput;

    Player rePlayer;

    public bool canMove;
    float cursorAngle;

    bool canShoot;

    public float invincTimer, invincLength;
    public bool isInvincible;

    public bool isDead;

    public GameObject uiLoadingContainer;
    
    // Start is called before the first frame update
    void Start()
    {
        charContr = GetComponent<CharacterController2D>();
        rePlayer = ReInput.players.GetPlayer(0);
        cursor = transform.GetChild(0).GetComponent<SpriteRenderer>();
        witchRenderer = GetComponent<SpriteRenderer>();

        canMove = true;
        aimInput = new Vector3(0, -1, 0);

        uiLoadingContainer.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (rePlayer.GetAnyButtonDown() && !GameManager.instance.gameInProgress)
        {
            GameManager.instance.GameStart();
            uiLoadingContainer.SetActive(false);
        }

        if (!GameManager.instance.gameInProgress || FloorManager.instance.isSwitchingFloors) return;

        ProcessInputs();
        RotateCursor();

        Shoot();

        if (isInvincible)
        {
            witchRenderer.color = new Color(1, 1, 1, 0.4f);

            invincTimer += Time.deltaTime;
            if (invincTimer >= invincLength)
            {
                invincTimer = 0;
                isInvincible = false;
            }
        } else
        {
            witchRenderer.color = Color.white;
        }        
    }

    public void Hurt()
    {
        if (isInvincible) return;
        
        PlayerManager.instance.playerCurrHearts -= 1;

        if (PlayerManager.instance.playerCurrHearts <= 0)
        {
            isDead = true;
        } else
        {
            isInvincible = true;
        }
    }

    public void Shoot()
    {
        if (!GameManager.instance.gameInProgress) return;

        if (rePlayer.GetButton("A") && canShoot)
        {
            CreateShot();

            PlayerManager.instance.playerWandTimer = PlayerManager.instance.playerWand.baseCooldown;
            canShoot = false;

            PlayerManager.instance.UpdateHUD();
        }

        if (!canShoot)
        {
            PlayerManager.instance.DecreaseWandTimer();

            if (PlayerManager.instance.playerWandTimer < 0)
            {
                canShoot = true;
            }

            PlayerManager.instance.UpdateHUD();
        }
    }

    void CreateShot()
    {
        switch (PlayerManager.instance.playerWand.id)
        {
            case WandTypes.NORMAL:
            case WandTypes.LARGE:
            case WandTypes.THROUGH:

                ShotController shotNormal = Instantiate<GameObject>(Resources.Load("Shot") as GameObject, cursor.transform.position + (aimInput.normalized), Quaternion.identity).GetComponent<ShotController>();

                shotNormal.velocity = aimInput.normalized;
                break;
            case WandTypes.TRIPLE:

                ShotController shotTripleA = Instantiate<GameObject>(Resources.Load("Shot") as GameObject, cursor.transform.position + (aimInput.normalized), Quaternion.identity).GetComponent<ShotController>();
                ShotController shotTripleB = Instantiate<GameObject>(Resources.Load("Shot") as GameObject, cursor.transform.position + (aimInput.normalized), Quaternion.identity).GetComponent<ShotController>();
                ShotController shotTripleC = Instantiate<GameObject>(Resources.Load("Shot") as GameObject, cursor.transform.position + (aimInput.normalized), Quaternion.identity).GetComponent<ShotController>();

                Vector2 aimNormal = aimInput.normalized;
                Vector2 aim = aimInput;

                Vector2 aAim = Quaternion.AngleAxis(-35, Vector3.forward) * aimNormal;
                Vector2 cAim = Quaternion.AngleAxis(35, Vector3.forward) * aimNormal;

                shotTripleA.velocity = aAim.normalized;
                shotTripleB.velocity = aimNormal;
                shotTripleC.velocity = cAim.normalized;

                break;
            default:
                break;
        }
    }

    void RotateCursor()
    {
        if (aimInput.x != 0 || aimInput.y != 0)
        {
            cursorAngle = Mathf.Atan2(aimInput.y, aimInput.x) * Mathf.Rad2Deg;
        }

        cursor.transform.rotation = Quaternion.Euler(new Vector3(0, 0, cursorAngle));
    }

    void ProcessInputs()
    {
        Vector3 tempAimInput = new Vector3(rePlayer.GetAxis("AimX"), rePlayer.GetAxis("AimY"));
        if (tempAimInput != Vector3.zero) aimInput = tempAimInput;

        moveInput = new Vector2(rePlayer.GetAxisRaw("MoveX"), rePlayer.GetAxisRaw("MoveY")).normalized;

        if (moveInput.x != 0)
        {
            moveSpeed.x = GameUtility.Approach(moveSpeed.x, PlayerManager.instance.playerMaxSpeed, acceleration);
        }
        else
        {
            moveSpeed.x = GameUtility.Approach(moveSpeed.x, 0, acceleration);
        }

        if (moveInput.y != 0)
        {
            moveSpeed.y = GameUtility.Approach(moveSpeed.y, PlayerManager.instance.playerMaxSpeed, acceleration);
        }
        else
        {
            moveSpeed.y = GameUtility.Approach(moveSpeed.y, 0, acceleration);
        }

        velocity = moveInput * moveSpeed;

    }

    private void FixedUpdate()
    {
        if (canMove) charContr.move(velocity * Time.deltaTime);
    }
}
