﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31;

public class ShotController : MonoBehaviour
{
    CharacterController2D charContr;

    public Vector2 velocity;
    public SpriteRenderer spr;
    public ProjectileTypes projType;

    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();

        spr.sprite = PlayerManager.instance.playerWand.GetSprite();

        charContr = GetComponent<CharacterController2D>();
        charContr.onTriggerEnterEvent += OnTriggerEnterEvent;
        charContr.onControllerCollidedEvent += OnShotCollide;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameUtility.InView(transform.position)) Destroy(gameObject);
    }

    private void FixedUpdate()
    {
        charContr.move(velocity * (projType == ProjectileTypes.PLAYER? PlayerManager.instance.GetWandSpeed() : 1) * Time.deltaTime);
    }

    public void OnTriggerEnterEvent(Collider2D col)
    {
        if (projType == ProjectileTypes.PLAYER)
        {
            if (col.tag != "Enemy") return;

            EnemyController enemy = col.GetComponent<EnemyController>();
            enemy.Hurt(PlayerManager.instance.baseDMG);

            if (PlayerManager.instance.playerWand.id == WandTypes.THROUGH) return;
            Destroy(gameObject);
        } else
        {
            if (col.tag != "Player") return;

            PlayerController player = col.GetComponent<PlayerController>();
            player.Hurt();

            Destroy(gameObject);

        }
    }

    public void OnShotCollide(RaycastHit2D hit)
    {
        if (PlayerManager.instance.playerWand.id == WandTypes.THROUGH && projType == ProjectileTypes.PLAYER) return;
        Destroy(gameObject);
    }
}